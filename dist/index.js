/*!
 * adelt-vue-toast v0.0.12
 * (c) Jonas Looser
 * Released under the ISC License.
 */
'use strict';

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var script = {
  name: 'adelt-toast',
  props: {
    toast: {
      type: Object,
      required: true
    },
    success: {
      type: String,
      "default": '#38c172'
    },
    danger: {
      type: String,
      "default": '#dd3333'
    },
    info: {
      type: String,
      "default": '#0098DB'
    },
    warn: {
      type: String,
      "default": '#ff8800'
    }
  },
  data: function data() {
    return {
      percent: '0%',
      count: 0
    };
  },
  mounted: function mounted() {
    this.count++;
  },
  methods: {
    tick: function tick() {
      this.percent = this.toast.lifetime * 100 + '%';
      if (this.toast.lifetime > 1) this.$emit('remove');
    },
    perform: function perform(action) {
      if (typeof action === 'string') {
        eval(action);
      } else {
        action(this);
      }
    },
    remove: function remove() {
      this.$emit('remove');
    }
  },
  computed: {
    progress: function progress() {
      return this.toast.lifetime * 100 + '%';
    },
    style: function style() {
      var result = {};
      this.count; // force recompute since we need to after the component is mounted

      if (!this.$el) {
        return result;
      }

      if (this.$el.classList.contains('outline')) {
        if (this.$el.classList.contains('success')) {
          result.borderColor = this.success;
        } else if (this.$el.classList.contains('warn')) {
          result.borderColor = this.warn;
        } else if (this.$el.classList.contains('danger')) {
          result.borderColor = this.danger;
        } else if (this.$el.classList.contains('info')) {
          result.borderColor = this.info;
        }
      } else {
        result.color = 'white';

        if (this.$el.classList.contains('success')) {
          result.backgroundColor = this.success;
        } else if (this.$el.classList.contains('warn')) {
          result.backgroundColor = this.warn;
        } else if (this.$el.classList.contains('danger')) {
          result.backgroundColor = this.danger;
        } else if (this.$el.classList.contains('info')) {
          result.backgroundColor = this.info;
        } else {
          result.color = 'inherit';
        }
      }

      return result;
    },
    progressStyle: function progressStyle() {
      var result = {};
      this.count;

      if (!this.$el) {
        return result;
      }

      if (this.$el.classList.contains('outline')) {
        if (this.$el.classList.contains('success')) {
          result.backgroundColor = this.success;
        } else if (this.$el.classList.contains('warn')) {
          result.backgroundColor = this.warn;
        } else if (this.$el.classList.contains('danger')) {
          result.backgroundColor = this.danger;
        } else if (this.$el.classList.contains('info')) {
          result.backgroundColor = this.info;
        }
      }

      result['animation-duration'] = this.toast.lifespan + 'ms';
      return result;
    }
  }
};

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
    if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
    }
    // Vue.extend constructor export interop.
    const options = typeof script === 'function' ? script.options : script;
    // render functions
    if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true;
        // functional template
        if (isFunctionalTemplate) {
            options.functional = true;
        }
    }
    // scopedId
    if (scopeId) {
        options._scopeId = scopeId;
    }
    let hook;
    if (moduleIdentifier) {
        // server build
        hook = function (context) {
            // 2.3 injection
            context =
                context || // cached call
                    (this.$vnode && this.$vnode.ssrContext) || // stateful
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
            // 2.2 with runInNewContext: true
            if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                context = __VUE_SSR_CONTEXT__;
            }
            // inject component styles
            if (style) {
                style.call(this, createInjectorSSR(context));
            }
            // register component module identifier for async chunk inference
            if (context && context._registeredComponents) {
                context._registeredComponents.add(moduleIdentifier);
            }
        };
        // used by ssr in case component is cached and beforeCreate
        // never gets called
        options._ssrRegister = hook;
    }
    else if (style) {
        hook = shadowMode
            ? function (context) {
                style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
            }
            : function (context) {
                style.call(this, createInjector(context));
            };
    }
    if (hook) {
        if (options.functional) {
            // register for functional component in vue file
            const originalRender = options.render;
            options.render = function renderWithStyleInjection(h, context) {
                hook.call(context);
                return originalRender(h, context);
            };
        }
        else {
            // inject component registration as beforeCreate hook
            const existing = options.beforeCreate;
            options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
    }
    return script;
}

const isOldIE = typeof navigator !== 'undefined' &&
    /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
function createInjector(context) {
    return (id, style) => addStyle(id, style);
}
let HEAD;
const styles = {};
function addStyle(id, css) {
    const group = isOldIE ? css.media || 'default' : id;
    const style = styles[group] || (styles[group] = { ids: new Set(), styles: [] });
    if (!style.ids.has(id)) {
        style.ids.add(id);
        let code = css.source;
        if (css.map) {
            // https://developer.chrome.com/devtools/docs/javascript-debugging
            // this makes source maps inside style tags work properly in Chrome
            code += '\n/*# sourceURL=' + css.map.sources[0] + ' */';
            // http://stackoverflow.com/a/26603875
            code +=
                '\n/*# sourceMappingURL=data:application/json;base64,' +
                    btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) +
                    ' */';
        }
        if (!style.element) {
            style.element = document.createElement('style');
            style.element.type = 'text/css';
            if (css.media)
                style.element.setAttribute('media', css.media);
            if (HEAD === undefined) {
                HEAD = document.head || document.getElementsByTagName('head')[0];
            }
            HEAD.appendChild(style.element);
        }
        if ('styleSheet' in style.element) {
            style.styles.push(code);
            style.element.styleSheet.cssText = style.styles
                .filter(Boolean)
                .join('\n');
        }
        else {
            const index = style.ids.size - 1;
            const textNode = document.createTextNode(code);
            const nodes = style.element.childNodes;
            if (nodes[index])
                style.element.removeChild(nodes[index]);
            if (nodes.length)
                style.element.insertBefore(textNode, nodes[index]);
            else
                style.element.appendChild(textNode);
        }
    }
}

/* script */
var __vue_script__ = script;
/* template */

var __vue_render__ = function __vue_render__() {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('div', {
    staticClass: "adelt-toast",
    style: _vm.style
  }, [_c('div', {
    staticClass: "title",
    domProps: {
      "innerHTML": _vm._s(_vm.toast.title)
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "actions"
  }, [_vm._l(_vm.toast.actions, function (action, i) {
    return _c('div', {
      key: i,
      "class": action["class"],
      attrs: {
        "title": action.title,
        "tabindex": "0"
      },
      on: {
        "click": function click($event) {
          return _vm.perform(action.action);
        }
      }
    }, [_vm._v("\n            " + _vm._s(action.title) + "\n        ")]);
  }), _vm._v(" "), _vm.toast.closable ? _c('div', {
    staticClass: "close",
    attrs: {
      "tabindex": "0"
    },
    on: {
      "click": function click($event) {
        return _vm.$emit('remove');
      }
    }
  }, [_vm._v("\n            x\n        ")]) : _vm._e()], 2), _vm._v(" "), _vm.toast.content ? _c('div', {
    staticClass: "content",
    domProps: {
      "innerHTML": _vm._s(_vm.toast.content)
    }
  }) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "progress",
    style: _vm.progressStyle
  })]);
};

var __vue_staticRenderFns__ = [];
/* style */

var __vue_inject_styles__ = function __vue_inject_styles__(inject) {
  if (!inject) return;
  inject("data-v-26a507a7_0", {
    source: ".adelt-toast[data-v-26a507a7]{width:100%;display:grid;grid-template-areas:\"title actions\" \"content content\" \"progress progress\";grid-gap:0;grid-template-columns:1fr auto;grid-template-rows:auto auto;background-color:#fff;border:1px solid #00000022;box-shadow:0 3px 6px #00000066;color:#000000aa}.adelt-toast>.title[data-v-26a507a7]{grid-area:title;padding:12px 6px 4px;min-height:24px;line-height:24px}.adelt-toast>.actions[data-v-26a507a7]{grid-area:actions;display:flex}.adelt-toast>.actions>*[data-v-26a507a7]{height:40px;line-height:40px;width:40px;cursor:pointer;text-align:center;outline:0}.adelt-toast>.actions[data-v-26a507a7]>:focus,.adelt-toast>.actions[data-v-26a507a7]>:hover{background-color:#00000033}.adelt-toast>.actions[data-v-26a507a7]>:active{background-color:#00000066}.adelt-toast>.content[data-v-26a507a7]{grid-area:content;padding:0 6px 8px;font-size:small}.adelt-toast>.progress[data-v-26a507a7]{grid-area:progress;height:4px;background-color:#00000066;position:relative;animation:progress-animation-data-v-26a507a7 linear;animation-fill-mode:forwards;animation-play-state:running;margin-top:8px}@keyframes progress-animation-data-v-26a507a7{0%{width:0}100%{width:100%}}.adelt-toast.outline.success[data-v-26a507a7]{border-color:#38c172}.adelt-toast.outline.success>.progress[data-v-26a507a7]{background-color:#38c172}.adelt-toast.outline.danger[data-v-26a507a7]{border-color:#d33}.adelt-toast.outline.danger>.progress[data-v-26a507a7]{background-color:#d33}.adelt-toast.outline.warn[data-v-26a507a7]{border-color:#f80}.adelt-toast.outline.warn>.progress[data-v-26a507a7]{background-color:#f80}.adelt-toast.outline.info[data-v-26a507a7]{border-color:#0098db}.adelt-toast.outline.info>.progress[data-v-26a507a7]{background-color:#0098db}.adelt-toast:not(.outline).danger[data-v-26a507a7],.adelt-toast:not(.outline).info[data-v-26a507a7],.adelt-toast:not(.outline).success[data-v-26a507a7],.adelt-toast:not(.outline).warn[data-v-26a507a7]{color:#fff}.adelt-toast:not(.outline).success[data-v-26a507a7]{background-color:#38c172}.adelt-toast:not(.outline).danger[data-v-26a507a7]{background-color:#d33}.adelt-toast:not(.outline).warn[data-v-26a507a7]{background-color:#f80}.adelt-toast:not(.outline).info[data-v-26a507a7]{background-color:#0098db}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


var __vue_scope_id__ = "data-v-26a507a7";
/* module identifier */

var __vue_module_identifier__ = undefined;
/* functional template */

var __vue_is_functional_template__ = false;
/* style inject SSR */

/* style inject shadow dom */

var __vue_component__ = /*#__PURE__*/normalizeComponent({
  render: __vue_render__,
  staticRenderFns: __vue_staticRenderFns__
}, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, false, createInjector, undefined, undefined);

//
var script$1 = {
  name: 'adelt-toaster',
  components: {
    'adelt-toast': __vue_component__
  },
  props: {
    toast_time: {
      type: [Number, String],
      "default": 8000
    },
    toast_class: {
      type: String,
      "default": ''
    },
    toast_closable: {
      type: Boolean,
      "default": true
    },
    transition: {
      type: String,
      "default": 'adelt-fade'
    },
    toasts: {
      type: Array,
      "default": function _default() {
        return [];
      }
    },
    direction: {
      type: String,
      "default": 'down'
    },
    top: {
      type: Boolean,
      "default": false
    },
    bottom: {
      type: Boolean,
      "default": false
    },
    left: {
      type: Boolean,
      "default": false
    },
    right: {
      type: Boolean,
      "default": false
    },
    success: {
      type: String,
      "default": '#38c172'
    },
    danger: {
      type: String,
      "default": '#dd3333'
    },
    info: {
      type: String,
      "default": '#0098DB'
    },
    warn: {
      type: String,
      "default": '#ff8800'
    }
  },
  data: function data() {
    return {
      toaster: [],
      allToasts: [],
      lastTick: 0,
      toasting: true
    };
  },
  watch: {
    toasts: function toasts(_toasts) {
      //console.log('toasts changed');
      if (_toasts.length) {
        //console.log('new toast detected');
        this.add(_toasts.slice(-1)[0]);
      }
    }
  },
  computed: {
    classes: function classes() {
      var result = {};
      result['direction-' + this.direction] = true;
      result['position-top'] = !!this.top;
      result['position-bottom'] = !!this.bottom;
      result['position-left'] = !!this.left;
      result['position-right'] = !!this.right;
      result['toasting'] = this.toasting;
      result['has-toasts'] = this.toaster.length > 0;
      return result;
    }
  },
  methods: {
    add: function add(toast) {
      if (typeof toast === 'string') {
        toast = {
          title: toast
        };
      }

      var nextToast = Object.assign({}, toast); //console.log('adding toast', toast);

      if (!nextToast.hasOwnProperty('lifespan')) {
        nextToast.lifespan = this.toast_time;
      }

      if (!nextToast.hasOwnProperty('title')) {
        nextToast.title = '';
      }

      if (!nextToast.hasOwnProperty('content')) {
        nextToast.content = '';
      }

      if (!nextToast.hasOwnProperty('actions')) {
        nextToast.actions = [];
      }

      if (!nextToast.hasOwnProperty('closable')) {
        nextToast.closable = this.toast_closable;
      }

      if (!nextToast.hasOwnProperty('class')) {
        nextToast["class"] = this.toast_class;
      }

      nextToast.counter = this.allToasts.length;
      nextToast.life = 0;
      nextToast.toastedAt = performance.now();
      nextToast.toasting = true;
      this.toaster.push(nextToast);
      this.allToasts.push(nextToast);
      return nextToast;
    },
    remove: function remove(toast) {
      this.toaster = this.toaster.filter(function (t) {
        return t.counter !== toast.counter;
      });
      this.toasting = true;

      if (toast.onRemove && typeof toast.onRemove === 'function') {
        toast.onRemove();
      }
    },
    tick: function tick() {
      var delta = performance.now() - this.lastTick;
      this.lastTick += delta;
      var minAge = null;

      if (this.toasting) {
        this.toaster.forEach(function (toast) {
          toast.life += delta;
          toast.lifetime = toast.lifespan > 0 ? toast.life / toast.lifespan : 0;
          toast.toasting = toast.lifetime <= 1;
          minAge = !minAge ? toast.lifetime : Math.min(minAge, toast.lifetime);

          if (toast.toasting) ;
        });

        if (this.$refs.toasts) {
          this.$refs.toasts.forEach(function (component) {
            return component.tick();
          });
        }

        if (minAge && minAge > 2) {
          //console.log('min age exceeded', minAge);
          this.toaster = [];
        }
      } //this.toaster = nextToasts;


      requestAnimationFrame(this.tick);
    },
    hoverStart: function hoverStart() {
      this.toasting = false;
    },
    hoverEnd: function hoverEnd() {
      this.toasting = true;
    }
  },
  beforeMount: function beforeMount() {//for (const i in new Range(10)) {
    //    this.add({
    //        title: i,
    //    });
    //}
  },
  mounted: function mounted() {
    this.lastTick = performance.now();
    this.tick();
  }
};

/* script */
var __vue_script__$1 = script$1;
/* template */

var __vue_render__$1 = function __vue_render__() {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('div', {
    staticClass: "adelt-toaster",
    "class": _vm.classes,
    on: {
      "mouseover": _vm.hoverStart,
      "mouseenter": _vm.hoverStart,
      "mouseout": _vm.hoverEnd,
      "mouseleave": _vm.hoverEnd
    }
  }, [_c('transition-group', {
    staticClass: "toasts",
    attrs: {
      "name": "adelt-toast",
      "tag": "div",
      "appear": ""
    }
  }, _vm._l(_vm.toaster, function (toast) {
    return _c('div', {
      key: 'holder_' + toast.counter,
      staticClass: "toast-holder"
    }, [_c('adelt-toast', {
      key: 'toast_' + toast.counter,
      ref: 'toasts',
      refInFor: true,
      "class": toast["class"],
      attrs: {
        "toast": toast,
        "success": _vm.success,
        "warn": _vm.warn,
        "danger": _vm.danger,
        "info": _vm.info
      },
      on: {
        "remove": function remove($event) {
          return _vm.remove(toast);
        }
      }
    })], 1);
  }), 0)], 1);
};

var __vue_staticRenderFns__$1 = [];
/* style */

var __vue_inject_styles__$1 = function __vue_inject_styles__(inject) {
  if (!inject) return;
  inject("data-v-6477bf40_0", {
    source: ".adelt-toaster[data-v-6477bf40]{font-family:sans-serif;width:100%;max-width:400px;padding:12px;overflow:hidden}.adelt-toaster>.toasts[data-v-6477bf40]{position:relative;display:flex}.adelt-toaster>.toasts>.toast-holder[data-v-6477bf40]{padding:4px 0}.adelt-toaster>.toasts>.toast-holder[data-v-6477bf40]:empty{padding:0}.adelt-toaster.direction-down>.toasts[data-v-6477bf40]{flex-direction:column}.adelt-toaster.direction-up>.toasts[data-v-6477bf40]{flex-direction:column-reverse}.adelt-toaster.position-bottom[data-v-6477bf40],.adelt-toaster.position-left[data-v-6477bf40],.adelt-toaster.position-right[data-v-6477bf40],.adelt-toaster.position-top[data-v-6477bf40]{position:absolute}.adelt-toaster.position-left[data-v-6477bf40]{left:0}.adelt-toaster.position-right[data-v-6477bf40]{right:0}.adelt-toaster.position-top[data-v-6477bf40]{top:0}.adelt-toaster.position-bottom[data-v-6477bf40]{bottom:0}.adelt-toaster .adelt-toast-move[data-v-6477bf40]{transition:transform 1s cubic-bezier(0,.5,.5,1)}.adelt-toaster .adelt-toast-enter-active[data-v-6477bf40],.adelt-toaster .adelt-toast-leave-active[data-v-6477bf40]{transition:opacity .3s cubic-bezier(0,.5,.5,1)}.adelt-toaster .adelt-toast-enter[data-v-6477bf40],.adelt-toaster .adelt-toast-leave-to[data-v-6477bf40]{opacity:0}.adelt-toaster .adelt-toast-enter-to[data-v-6477bf40],.adelt-toaster .adelt-toast-leave[data-v-6477bf40]{opacity:1}@keyframes from-right-data-v-6477bf40{0%{margin-right:-500px}100%{margin-right:0}}.adelt-toaster[data-v-6477bf40],.adelt-toaster *[data-v-6477bf40]{box-sizing:border-box}",
    map: undefined,
    media: undefined
  }), inject("data-v-6477bf40_1", {
    source: ".adelt-toaster:hover>.toasts>.toast-holder>.adelt-toast>.progress{animation-play-state:paused}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


var __vue_scope_id__$1 = "data-v-6477bf40";
/* module identifier */

var __vue_module_identifier__$1 = undefined;
/* functional template */

var __vue_is_functional_template__$1 = false;
/* style inject SSR */

/* style inject shadow dom */

var __vue_component__$1 = /*#__PURE__*/normalizeComponent({
  render: __vue_render__$1,
  staticRenderFns: __vue_staticRenderFns__$1
}, __vue_inject_styles__$1, __vue_script__$1, __vue_scope_id__$1, __vue_is_functional_template__$1, __vue_module_identifier__$1, false, createInjector, undefined, undefined);

function install(Vue) {
  if (install.installed) return;
  install.installed = true;
  Vue.component('adelt-toaster', __vue_component__$1);
}

var plugin = {
  install: install
};
var GlobalVue = null;

if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.vue;
}

if (GlobalVue) {
  GlobalVue.use(plugin);
}

__vue_component__$1.install = install;

module.exports = __vue_component__$1;
