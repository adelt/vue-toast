import Toaster from "./toaster.vue";

function install(Vue) {
  if (install.installed) return
  install.installed = true
  Vue.component('adelt-toaster', Toaster)
}

const plugin = {
  install
}

let GlobalVue = null
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue
} else if (typeof global !== 'undefined') {
  GlobalVue = global.vue
}

if (GlobalVue) {
  GlobalVue.use(plugin)
}

Toaster.install = install

export default Toaster;
