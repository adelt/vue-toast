# Adelt Vue Toaster
This is an useful Toaster-Vue-Component to use in your vue application.

## Installation
To install the package, call the following command:
```
npm install --save adelt-vue-toast
```

To use the package, import it in your project and register it to your Vue instance:
```
import Toster from 'adelt-vue-toast';
Vue.use(Toaster);  // Vue is your Vue-instance
```

## Usage
To use the toaster, simply add the component anywhere to your project:
```
<adelt-toaster />
```

There are several options available that will help you use the toaster right away. For example, props for the position of the toaster and the direction of the toasts:
```
<adelt-toaster bottom right direction="up" />
```

To add a toast, simply call the `add(toast)` method of the component (you can get a handle to the component by using a `ref=...` attribute in the template), or
by passing in a `toasts` array:
```
<adelt-toaster :toasts="my_toasts" ref="toaster" />
```

Note: Every time you append toasts to the my_toast array, the last toast of it will be added to the toaster. So, remember to only add one toast at a time
and also don't remove toasts from that array. We are aware of that problem it my be subject to change in future releases.

## Toast configuration
Toasts can have several attributes that can ease your life. When adding a toast, the object can have the following attributes:

`title`: The HTML title of the toast
`content`: The HTML content of the toast
`lifespan`: Sets how long the toast will be visible in milliseconds. 0 equals to infinite.
`actions`: An array with actions (buttons), other than the close button.
`onRemove`: A function that gets called when the toast is dismissed or removed
`closable`: A boolean, that when false, makes the toast not user closable

